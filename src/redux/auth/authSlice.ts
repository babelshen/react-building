import { createSlice } from "@reduxjs/toolkit";
import { signInUserRequest, signOut, signUpUserRequest } from "./action.creators";

const initialState = {
    loadingSignUp: false,
    errorSignUp: null,
    successSignUp: false,
    loadingSignIn: false,
    errorSignIn: null,
    successSignIn: false,
    buildings: []
};

const authSlice = createSlice({
    name: "auth",
    initialState: initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(signUpUserRequest.pending, (state) => {
                state.loadingSignUp = true;
                state.errorSignUp = null;
                state.successSignUp = false;
            })
            .addCase(signUpUserRequest.fulfilled, (state) => {
                state.loadingSignUp = false;
                state.successSignUp = true;
            })
            .addCase(signUpUserRequest.rejected, (state) => {
                state.loadingSignUp = false;
                state.successSignUp = false;
            });
        builder
            .addCase(signInUserRequest.pending, (state) => {
                state.loadingSignIn = true;
                state.errorSignIn = null;
                state.successSignIn = false;
            })
            .addCase(signInUserRequest.fulfilled, (state, action) => {
                state.loadingSignIn = false;
                state.successSignIn = true;
                state.buildings = action.payload.buildings.data;
            })
            .addCase(signInUserRequest.rejected, (state) => {
                state.loadingSignIn = false;
                state.successSignIn = false;
            });
        builder
            .addCase(signOut, () => {
                return initialState;
            });
    },
});

export const {} = authSlice.actions;

export default authSlice.reducer;