import { createAction, createAsyncThunk } from "@reduxjs/toolkit";
import { signInApi, signUpApi } from "../../config/api/auth";
import { getMeApi } from "../../config/api/user";
import { loadData } from "../../config/api/buildings";

export const signUpUserRequest = createAsyncThunk(
    "/auth/sign-up",
    async ({ email, password }: { email: string; password: string }, { rejectWithValue }) => {
        try {
            const response = await signUpApi(email, password);
            return response.data;
        } catch (error) {
            return rejectWithValue(error);
        }
    }
);

export const signInUserRequest = createAsyncThunk(
    "/auth/sign-in",
    async ({ email, password }: { email: string; password: string }, { rejectWithValue }) => {
        try {
            const { data } = await signInApi(email, password);

            localStorage.setItem("userToken", data.accessToken);

            const result = await getMeApi();
            
            localStorage.setItem("userId", result.data.id);
            localStorage.setItem("userEmail", result.data.email);

            const buildings = await loadData();
            return {
                data,
                buildings,
            };
        } catch (error) {
            return rejectWithValue(error);
        }
    }
);

export const signOut = createAction('SIGN_OUT');
