import { apiCaller } from "../api-caller";
import { AxiosResponse } from "axios";

export const signUpApi = async (email: string, password: string): Promise<AxiosResponse> =>
    apiCaller({
        method: "POST",
        url: "/auth/sign-up",
        data: {
            email,
            password,
        },
    });

export const signInApi = async (email: string, password: string): Promise<AxiosResponse> =>
    apiCaller({
        method: "POST",
        url: "/auth/sign-in",
        data: {
            email,
            password,
        },
    });