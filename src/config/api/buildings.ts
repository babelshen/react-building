import { apiCaller } from "../api-caller";
import { AxiosResponse } from "axios";

export const loadData = async (): Promise<AxiosResponse> =>
    apiCaller({
        method: "GET",
        url: "/buildings",
    });