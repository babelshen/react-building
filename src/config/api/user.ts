import { AxiosResponse } from "axios";
import { apiCallerId } from "../api-caller";

export const getMeApi = async (): Promise<AxiosResponse> =>
    apiCallerId({
        method: "GET",
        url: "/users/me",
    });
