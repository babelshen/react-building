export const theme = {
    colors: {
        main: 'white',
        mainDark: '#172234',
        background: '#F2F2F2',
        error: 'red',
        input: '#E0E0E0',
        label: 'black',
        secondary: '#B29F7E',
    },
    fonts: {
        merriweather: "'Merriweather', serif",
        lato: "'Lato', sans-serif",
    },
    borderRadius: {
        small: '5px',
        big: '8px',
    },
    fontSizes: {
        main: '14px',
        button: '16px',
        itemDescription: '18px',
        itemTitle: '20px',
        mainDescription: '24px',
        title: '28px',
        siteTitleMob: '34px',
        siteTitle: '64px',
    },
    fontWeight: {
        bold: 700,
        medium: 600,
        regular: 400,
    }
}
