import { createBrowserRouter, createRoutesFromElements, Route } from "react-router-dom";
import MainPage from '../pages/MainPage';
import Header from "../components/BuildedComponents/Header";
import LoginPage from "../pages/LoginPage";
import SignUpPage from "../pages/SignUpPage";
import ErrorPage from "../pages/ErrorPage";

const router = createBrowserRouter(createRoutesFromElements(
    <Route path="/" element={<Header />}>
        <Route index element={<MainPage />} />  
        <Route path="login" element={<LoginPage />} />  
        <Route path="signup" element={<SignUpPage />} />     
        <Route path="*" element={<ErrorPage />} />
    </Route>
));

export default router;
