import SectionTitleStyled from '../../StyledComponents/SectionTitleStyled';
import GridStyled from '../../StyledComponents/GridStyled';
import OpenDealArticle from '../OpenDealArticle';
import style from './OpenDealSection.module.css';
import { useEffect, useState } from 'react';
import { loadData } from '../../../config/api/buildings';
import { IInfo } from './interface';
import { useAppSelector } from '../../../config/hooks';

const OpenDealSection = () => {

    const [info, setInfo] = useState<IInfo[]>([]);
    const buildings = useAppSelector((state) => state.auth.buildings);

    useEffect(() => {
        if (buildings) {
            setInfo(buildings);
        }
        async function fetchData() {
            const result = await loadData();
            setInfo(result.data);
        }
        fetchData();
    }, [buildings]);

    return(
        <section className={style.section}>
            <SectionTitleStyled>Open Deals</SectionTitleStyled>
            <GridStyled>
                {info.map(item => (
                    <OpenDealArticle key={item.id} item={item} />
                ))}
            </GridStyled>
        </section>
    );
}

export default OpenDealSection;