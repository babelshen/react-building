import LoaderStyled from "../../StyledComponents/LoaderStyled";

const Loader = () => {
    return(
        <LoaderStyled>
            <img src='https://i16.servimg.com/u/f16/20/20/43/41/double10.gif' alt='Loader' title='Loading...' />
        </LoaderStyled>
    )
}

export default Loader;