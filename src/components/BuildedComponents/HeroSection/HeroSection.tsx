import HeroSectionStyled from '../../StyledComponents/HeroSectionStyled';
import AppWrapperStyled from '../../StyledComponents/AppWrapperStyled';
import TitleStyled from '../../StyledComponents/TitleStyled';
import DescriptionStyled from '../../StyledComponents/DescriptionStyled';
import ButtonStyled from '../../StyledComponents/ButtonStyled';

const HeroSection: React.FC = () => {
    return(
        <HeroSectionStyled>
            <AppWrapperStyled display='flex' direction='column' align='center' margin='0px 80px'>
                <TitleStyled>
                    The chemical  negatively charged
                </TitleStyled>
                <DescriptionStyled>
                    Numerous calculations predict, and experiments confirm, that the force field reflects the beam, while the mass defect is not formed. The chemical compound is negatively charged. Twhile the mass defect is 
                </DescriptionStyled>
                <ButtonStyled withbackground border='1px solid white' color='white' padding='10px 24px' radius='5px' fontSize='20px' lineheight='34px'>
                    Get Started
                </ButtonStyled>
            </AppWrapperStyled> 
        </HeroSectionStyled>
    );
}

export default HeroSection;