import React from 'react';
import { FieldError } from 'react-hook-form';
import ErrorMessageStyled from '../../StyledComponents/ErrorMessageStyled';
import FieldStyled from '../../StyledComponents/FieldStyled';
import LabelStyled from '../../StyledComponents/LabelStyled';
import InputWrapperStyled from '../../StyledComponents/InputWrapperStyled';
import InputStyled from '../../StyledComponents/InputStyled';

const Input = React.forwardRef(
  ({ type, label, placeholder, error, ...props}: { type: string; label?: string; placeholder: string; error?: FieldError;}, ref: React.Ref<HTMLInputElement>) => {

    return (
      <FieldStyled>
        {label && <LabelStyled>{label}</LabelStyled>}
        <InputWrapperStyled>
          <InputStyled
            ref={ref} 
            type={type} 
            placeholder={placeholder}
            {...props}
            />
        </InputWrapperStyled>
        {error ? <ErrorMessageStyled>{error.message || 'Error'}</ErrorMessageStyled> : false}
      </FieldStyled>
    );
  }
);

export default Input;