import HeaderStyled from '../../StyledComponents/HeaderStyled';
import MenuStyled from '../../StyledComponents/MenuStyled';
import AppWrapperStyled from '../../StyledComponents/AppWrapperStyled';
import ButtonStyled from '../../StyledComponents/ButtonStyled';
import { Outlet, useLocation } from 'react-router';
import { Link } from 'react-router-dom';
import { useAppDispatch } from '../../../config/hooks';
import { signOut } from '../../../redux/auth/action.creators';

const Header = () => {

  const location = useLocation();
  const isLoginPage = location.pathname === '/login';
  const isSignUpPage = location.pathname === '/signup';

  const dispatch = useAppDispatch();

  const handleClickSignOut = () => {
    localStorage.removeItem('userToken');
    localStorage.removeItem('userEmail');
    localStorage.removeItem('userId');
    dispatch(signOut());
  }

  return(
    <>
      <HeaderStyled>
        <MenuStyled>
          { isLoginPage || isSignUpPage
            ? false 
            : (<AppWrapperStyled display='flex' direction='row' justify='flex-end' gap='10px' margin='0 80px'>
              { localStorage.getItem('userToken') && localStorage.getItem('userId')
              ? <Link to='/'><ButtonStyled transparent border={'1px solid #B29F7E'} color={'#B29F7E'} onClick={handleClickSignOut}>Sign Out</ButtonStyled></Link>
              : (<>
                <Link to='/login'><ButtonStyled transparent border={'1px solid #B29F7E'} color={'#B29F7E'}>Log In</ButtonStyled></Link>
                <Link to='/signup'><ButtonStyled withbackground background={'#B29F7E'} color={'white'}>Sign Up</ButtonStyled></Link>
              </>)}
          </AppWrapperStyled>)}
        </MenuStyled>     
      </HeaderStyled>
      <Outlet />
    </>
  );
}

export default Header;