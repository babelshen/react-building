import ArticleDealStyled from '../../StyledComponents/ArticleDealStyled';
import InfoBlockStyled from '../../StyledComponents/InfoBlockStyled';
import DealTitleNameStyled from '../../StyledComponents/DealTitleNameStyled';
import AdditionalInfoItemStyled from '../../StyledComponents/AdditionalInfoItemStyled';
import DealAdditionalInfoItemStyled from '../../StyledComponents/DealAdditionalInfoItemStyled';
import style from './OpenDealArticle.module.css';
import { IItemProp } from './interface';

const OpenDealArticle: React.FC<IItemProp> = ({item}) => {
    return(
        <ArticleDealStyled>
                  <div className={style.item__image_wrapper}>
                    <img className={style.item__image} src={item.imageLink} alt={item.name} title={item.name} />
                  </div>
                  <InfoBlockStyled>
                    <DealTitleNameStyled>{item.name}</DealTitleNameStyled>
                    <AdditionalInfoItemStyled>
                      <DealAdditionalInfoItemStyled>{item.cost}</DealAdditionalInfoItemStyled>
                      <DealAdditionalInfoItemStyled>Tiket - {item.ticket}</DealAdditionalInfoItemStyled>
                      <DealAdditionalInfoItemStyled>Yield {item.yield}</DealAdditionalInfoItemStyled>
                      <DealAdditionalInfoItemStyled>Days left {item.days}</DealAdditionalInfoItemStyled>
                      <DealAdditionalInfoItemStyled>Sold {item.sold}</DealAdditionalInfoItemStyled>
                    </AdditionalInfoItemStyled>
                  </InfoBlockStyled>
              </ArticleDealStyled>
    )
}

export default OpenDealArticle;