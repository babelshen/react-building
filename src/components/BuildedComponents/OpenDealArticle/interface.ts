export interface IItemProp {
    item: {
        id: string;
        createdAt: string;
        updatedAt: string;
        name: string;
        imageLink: string;
        cost: string;
        days: number;
        yield: string;
        sold: string;
        ticket: string;
    };
}