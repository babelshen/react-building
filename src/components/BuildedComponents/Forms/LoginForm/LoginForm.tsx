import { SubmitHandler, useForm } from "react-hook-form";
import { Link, useNavigate } from 'react-router-dom';
import Input from "../../Input";
import FormStyled from "../../../StyledComponents/FormStyled";
import GuestTitle from "../../../StyledComponents/GuestTitle";
import ButtonStyled from "../../../StyledComponents/ButtonStyled";
import TipStyled from "../../../StyledComponents/TipStyled";
import style from './LoginForm.module.css';
import { signInUserRequest } from "../../../../redux/auth/action.creators";
import { useAppDispatch, useAppSelector } from "../../../../config/hooks";
import { useEffect } from "react";
import { ILoginForm } from "./interface";
import ErrorMessageStyled from '../../../StyledComponents/ErrorMessageStyled';

const LoginForm = () => {

    const { register, handleSubmit, formState: {errors}} = useForm<ILoginForm>({mode: 'onBlur'});

    const navigate = useNavigate();

    const dispatch = useAppDispatch();
    const isLoginLoading = useAppSelector((state) => state.auth.loadingSignIn);
    const isLoginSuccess = useAppSelector((state) => state.auth.successSignIn);
    const isLoginError = useAppSelector((state) => state.auth.errorSignIn);

    const onSubmit:SubmitHandler<ILoginForm> = (data) => {
        dispatch(signInUserRequest({ ...data }));
    };

    useEffect(() => {
        if (!isLoginLoading) {
            if (isLoginSuccess) {
                navigate('/');
            }
        }
    }, [isLoginLoading, isLoginSuccess, dispatch]);

    return (
        <FormStyled onSubmit={handleSubmit(onSubmit)}>
                <GuestTitle>Login</GuestTitle>
                <Input 
                    type="email" 
                    label="Email"  
                    placeholder="Email"
                    error = {errors.email}
                
                    {...register("email", {
                        required: 'This field must be filled in', 
                        minLength: {
                        value: 5,
                        message: 'Email is too short. Minimum characters: 5',
                        },
                        maxLength: {
                        value: 25,
                        message: 'Email is too long. Maximum characters: 25',
                        },
                        pattern: {
                        value: /^\S+@\S+$/i,
                        message: 'Invalid email',
                        }
                    })} 
                />

                <Input 
                    type='password' 
                    label="Password" 
                    placeholder="Password"
                    error = { errors.password }

                    {...register("password", {
                        required: 'This field must be filled in',
                        minLength: {
                        value: 6,
                        message: 'Password is too short. Minimum characters: 5',
                        },
                        maxLength: {
                        value: 18,
                        message: 'Password is too long. Maximum characters: 18',
                        },
                    })} 
                />

                <Link to='#' className={style.link__forgot}>Forgot password?</Link>

                {isLoginError ? <ErrorMessageStyled>Invalid E-mail or password</ErrorMessageStyled> : false}

                <ButtonStyled withbackground background={'#B29F7E'} color={'white'} width='100%' type="submit" onClick={handleSubmit(onSubmit)}>
                    Sign In
                </ButtonStyled>

                <TipStyled>Don’t have account? <Link to='/signup' className={style.link}>Sign Up</Link></TipStyled>
            </FormStyled>
    );
};

export default LoginForm