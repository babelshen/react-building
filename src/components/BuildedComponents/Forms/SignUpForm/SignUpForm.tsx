import { SubmitHandler, useForm } from "react-hook-form";
import FormStyled from "../../../StyledComponents/FormStyled";
import GuestTitle from "../../../StyledComponents/GuestTitle";
import Input from "../../Input";
import ButtonStyled from "../../../StyledComponents/ButtonStyled";
import { useNavigate } from "react-router";
import { useAppDispatch, useAppSelector } from "../../../../config/hooks";
import { signUpUserRequest } from "../../../../redux/auth/action.creators";
import { useEffect } from "react";
import { ISignUpForm } from "./interface";

const SignUpForm = () => {

    const { register, handleSubmit, formState: {errors}, getValues} = useForm<ISignUpForm>({mode: 'onBlur'});

    const navigate = useNavigate();

    const dispatch = useAppDispatch();
    const isSignUpLoading = useAppSelector((state) => state.auth.loadingSignUp);
    const isSignUpSuccess = useAppSelector((state) => state.auth.successSignUp);
    
    useEffect(() => {
        if (!isSignUpLoading) {
            if (isSignUpSuccess) {
                navigate('/login');
            }
        }
    }, [isSignUpLoading, isSignUpSuccess, dispatch]);

    const onSubmit:SubmitHandler<ISignUpForm> = (data) => {
        dispatch(signUpUserRequest(data));
        navigate('/login');
    };
    
    return(
        <FormStyled onSubmit={handleSubmit(onSubmit)}>
                <GuestTitle>Sign Up</GuestTitle>
                <Input 
                    type="email" 
                    label="Email"  
                    placeholder="Email"
                    error = {errors.email}
                
                    {...register("email", {
                        required: 'This field must be filled in', 
                        minLength: {
                        value: 5,
                        message: 'Email is too short. Minimum characters: 5',
                        },
                        maxLength: {
                        value: 25,
                        message: 'Email is too long. Maximum characters: 25',
                        },
                        pattern: {
                        value: /^\S+@\S+$/i,
                        message: 'Invalid email',
                        }
                    })} 
                />

                <Input 
                    type='password'
                    label="Password" 
                    placeholder="Password"
                    error={errors.password}
                    {...register("password", {
                        required: 'This field must be filled in',
                        minLength: {
                            value: 6,
                            message: 'Password is too short. Minimum characters: 5',
                        },
                        maxLength: {
                            value: 18,
                            message: 'Password is too long. Maximum characters: 18',
                        },
                        validate: (value) => {
                            const confirmPasswordValue = getValues("confirmPassword");
                            return value === confirmPasswordValue || "Passwords do not match";
                        },
                    })}
                />
                <Input 
                    type='password'
                    label="Confirm Password" 
                    placeholder="Confirm Password"
                    error={errors.confirmPassword}
                    {...register("confirmPassword", {
                        required: 'This field must be filled in',
                        minLength: {
                            value: 6,
                            message: 'Password is too short. Minimum characters: 5',
                        },
                        maxLength: {
                            value: 18,
                            message: 'Password is too long. Maximum characters: 18',
                        },
                        validate: (value) => {
                            const passwordValue = getValues("password");
                            return value === passwordValue || "Passwords do not match";
                        },
                    })}
                />

                <ButtonStyled withbackground background={'#B29F7E'} color={'white'} width='100%' type="submit" onClick={handleSubmit(onSubmit)}>
                    Sign Up
                </ButtonStyled>
            </FormStyled>
    )
}

export default SignUpForm;