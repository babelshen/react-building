import styled from 'styled-components';

const FieldStyled = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%; 
`

export default FieldStyled;