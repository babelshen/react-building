import styled from 'styled-components';

const SectionTitleStyled = styled.h2`
  color: ${props => props.theme.colors.secondary};
  font-family: ${props => props.theme.fonts.merriweather};
  font-size: ${props => props.theme.fonts.title};
  font-weight: ${props => props.theme.fontWeight.bold};
  line-height: 34px;
  margin-bottom: 20px;
`

export default SectionTitleStyled;