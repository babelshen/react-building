import styled from 'styled-components';

const InfoBlockStyled = styled.div`
  position: absolute;
  z-index: 1;
  bottom: 0;
  left: 0;
  right: 0;
  background: transparent;
`

export default InfoBlockStyled;