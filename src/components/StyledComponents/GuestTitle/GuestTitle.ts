import styled from 'styled-components';

const GuestTitle = styled.h2`
    color: ${props => props.theme.colors.mainDark};
    font-family: ${props => props.theme.fonts.merriweather};
    font-size: ${props => props.theme.fontSizes.title};
    font-weight: ${props => props.theme.fontWeight.bold};
    line-height: 34px;
`

export default GuestTitle;