import styled from 'styled-components';

const ArticleDealStyled = styled.article`
  position:relative;
  box-shadow: 0px 4px 4px 0px rgba(0, 0, 0, 0.15);
  border-radius: ${props => props.theme.borderRadius.small};
`

export default ArticleDealStyled;