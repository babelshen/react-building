import { styled } from "styled-components";

const LoaderStyled = styled.div`
    width: 100%;
    text-align: center;
`

export default LoaderStyled;