import styled from 'styled-components';

const DescriptionStyled = styled.h3`
  color: ${props => props.theme.colors.main};
  text-align: center;
  font-family: ${props => props.theme.fonts.lato};
  font-size: ${props => props.theme.fontSizes.mainDescription};
  font-weight: ${props => props.theme.fontWeight.regular};
  line-height: 32px;
  letter-spacing: -0.48px;
  background: transparent;
  max-width: 822px;
  margin: 10px 0 30px;

  @media (max-width: 730px) {
    font-size: ${props => props.theme.fontSizes.itemDescription};
  }
`

export default DescriptionStyled;