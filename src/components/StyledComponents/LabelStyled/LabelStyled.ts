import styled from 'styled-components';

const LabelStyled = styled.label`
    color: ${props => props.theme.colors.label};
    font-family: ${props => props.theme.fonts.merriweather};
    font-size: ${props => props.theme.fonts.main};
    font-weight: ${props => props.theme.fontWeight.bold};
    line-height: 20px;
    margin: 20px 0 3px;
`

export default LabelStyled;