import styled from 'styled-components';

const MenuStyled = styled.div`
  background: ${props => props.theme.colors.mainDark};
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  min-height: 80px;
`

export default MenuStyled;