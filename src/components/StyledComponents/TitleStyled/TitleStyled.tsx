import styled from 'styled-components';

const TitleStyled = styled.h1`
  color: ${props => props.theme.colors.main};
  text-align: center;
  font-family: ${props => props.theme.fonts.merriweather};
  font-size: ${props => props.theme.fontSizes.siteTitle};
  font-weight: ${props => props.theme.fontWeight.bold};
  line-height: 80px;
  background: transparent;
  margin-top: 297px;

  @media (max-width: 730px) {
    font-size: ${props => props.theme.fontSizes.siteTitleMob};
    line-height: 60px;
  }
` 

export default TitleStyled;