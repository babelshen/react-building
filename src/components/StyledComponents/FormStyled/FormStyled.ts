import styled from 'styled-components';

const FormStyled = styled.form`
    margin: auto;
    display: flex;
    flex-direction: column;
    padding: 0 15px;

    @media (max-width: 860px) and (min-width: 650px) {
        width: 45%;
    }

    @media (max-width: 650px) {
        width: 65%;
    }
`

export default FormStyled;