import { styled } from "styled-components";

const ErrorMessageStyled = styled.div`
    color: ${props => props.theme.colors.error};
    font-family: ${props => props.theme.fonts.lato};
    font-size: ${props => props.theme.fontSizes.main};
    font-weight: ${props => props.theme.fontWeight.medium};
    line-height: 22px;
    text-align: center;
    margin-top: 10px;
`

export default ErrorMessageStyled;