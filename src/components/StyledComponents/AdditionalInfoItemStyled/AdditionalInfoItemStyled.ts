import styled from 'styled-components';

const AdditionalInfoItemStyled = styled.div`
  display: grid;
  grid-template-rows: repeat(2, 1fr);
  grid-template-columns: repeat(3, 1fr);
  margin: 0 14px 20px;

  @media (max-width: 540px) {
    grid-template-columns: 1fr;
  }
`

export default AdditionalInfoItemStyled;