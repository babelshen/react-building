import styled from 'styled-components';

const InputWrapperStyled = styled.div`
    position: relative;
    display: inline-flex;
    width: 100%;
`

export default InputWrapperStyled;