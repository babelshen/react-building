import styled from 'styled-components';

const BackgroundImageStyled = styled.div`
    width: 65%;
    height: 943px;
    background-image: url('https://i16.servimg.com/u/f16/20/20/43/41/login-10.jpg');
    background-size: cover;
    background-repeat: no-repeat;
`
export default BackgroundImageStyled;