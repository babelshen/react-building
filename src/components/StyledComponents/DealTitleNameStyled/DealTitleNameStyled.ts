import styled from 'styled-components';

const DealTitleNameStyled = styled.h4`
  color: ${props => props.theme.colors.main};
  font-family: ${props => props.theme.fonts.merriweather};
  font-size: ${props => props.theme.fontSizes.itemTitle};
  font-weight: ${props => props.theme.fontWeight.bold};
  line-height: 34px;
  margin: 0 14px 5px;
`

export default DealTitleNameStyled;