import styled from 'styled-components';

const HeroSectionStyled = styled.section`
    background: url('https://i16.servimg.com/u/f16/20/20/43/41/hero10.png');
    background-repeat: no-repeat;
    background-size: cover;
    min-height: 1024px;

    &::before {
        content: '';
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 1024px;
        background: ${props => props.theme.colors.mainDark};
        opacity: 0.6;
    }`

export default HeroSectionStyled;