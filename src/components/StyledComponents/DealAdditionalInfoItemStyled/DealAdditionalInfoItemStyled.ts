import styled from 'styled-components';

const DealAdditionalInfoItemStyled = styled.span`
  color: ${props => props.theme.colors.main};
  font-family: ${props => props.theme.fontSizes.itemDescription};
  font-size: ${props => props.theme.fonts.lato};
  font-weight: ${props => props.theme.fontWeight.bold};
  line-height: 22px;
  max-width: 170px;

  @media (max-width: 660px) {
    font-size: ${props => props.theme.fontSizes.main};
  }

  &:nth-child(1) {
    order: 1;
  }
  
  &:nth-child(2) {
    order: 4;
  }
  
  &:nth-child(3) {
    order: 2;
  }
  
  &:nth-child(4) {
    order: 5;
    max-width: 156px;
  }
  
  &:nth-child(5) {
    order: 3;
  }
`

export default DealAdditionalInfoItemStyled;