import styled from 'styled-components';

const GridStyled = styled.div`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  gap: 10px;

  @media (max-width: 930px) {
    grid-template-columns: 1fr;
  }
`

export default GridStyled;