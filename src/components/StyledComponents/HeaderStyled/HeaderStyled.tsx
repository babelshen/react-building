import styled from 'styled-components';

const HeaderStyled = styled.header`
  position: relative;
  z-index: 1;
`

export default HeaderStyled;