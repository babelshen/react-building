import styled, { css } from 'styled-components';
import { IButtonProps } from './interface';

const ButtonStyled = styled.button<IButtonProps>`
  text-align: center;
  font-family: ${props => props.theme.fonts.merriweather};
  font-weight: ${props => props.theme.fontWeight.bold};
  margin: 18px 0;
  cursor: pointer;
  width: ${props => (props.transparent || props.withbackground) ? '160px' : 'auto'};

  ${props => props.transparent && css`
    border: ${props.border || 'none'};
    background: transparent;
    color: ${props.color || 'white'};
    padding: ${props.padding || '11px 0px'};
    border-radius: ${props.radius || '5px'};
    font-size: ${props.fontSize || '16px'};
    line-height: ${props.lineheight || '22px'};
  `}
  
  ${props => props.withbackground && css`
    border: ${props.border || 'none'};
    background: ${props.background || 'none'};
    color: ${props.color || 'white'};
    padding: ${props.padding || '11px 0px'};
    border-radius: ${props.radius || '5px'};
    font-size: ${props.fontSize || '16px'};
    line-height: ${props.lineheight || '22px'};
    width: ${props.width || '160px'};
  `}
`;

export default ButtonStyled;