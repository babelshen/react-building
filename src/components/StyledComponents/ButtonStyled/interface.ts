export interface IButtonProps {
    transparent?: boolean;
    withbackground?: boolean;
    border?: string;
    background?: string;
    color?: string;
    padding?: string;
    radius?: string;
    fontSize?: string;
    lineheight?: string;
    width?: string;
}