import styled from 'styled-components';

const InputStyled = styled.input`
    display: flex;
    width: 350px;
    padding: 14px 20px 12px 20px;
    justify-content: center;
    align-items: center;
    border-radius: ${props => props.theme.borderRadius.small};
    border: 2px solid ${props => props.theme.colors.input};
    background: ${props => props.theme.colors.input};

    &::placeholder {
        color: ${props => props.theme.colors.mainDark};
        font-family: ${props => props.theme.fonts.lato};
        font-size: ${props => props.theme.fonts.main};
        font-weight: ${props => props.theme.fontWeight.regular};
        line-height: 22px;
        opacity: 0.5;
    }
`
export default InputStyled;