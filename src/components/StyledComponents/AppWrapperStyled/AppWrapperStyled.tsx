import styled from 'styled-components';
import { IAppWrapperProps } from './interface';

const AppWrapperStyled = styled.div<IAppWrapperProps>`
  background: transparent;
  position: relative;
  display: ${props => props.display || 'block'};
  flex-direction: ${props => props.direction || 'none'};
  justify-content: ${props => props.justify || 'stretch'};
  align-items: ${props => props.align || 'stretch'};
  gap: ${props => props.gap || 0};
  margin: ${props => props.margin || 0};
  
  @media (max-width: 500px) {
    justify-content: center;
  }
`

export default AppWrapperStyled;