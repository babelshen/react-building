export interface IAppWrapperProps {
    display?: string;
    direction?: string;
    justify?: string;
    align?: string;
    gap?: string;
    margin?: string;
}