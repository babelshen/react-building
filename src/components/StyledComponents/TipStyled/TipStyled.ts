import styled from 'styled-components';

const TipStyled = styled.div`
    color: ${props => props.theme.colors.mainDark};
    text-align: center;
    font-family: ${props => props.theme.fonts.lato};
    font-size: ${props => props.theme.fonts.main};
    font-weight: ${props => props.theme.fontSizes.medium};
    line-height: 22px;
`

export default TipStyled;