import SignUpForm from "../../components/BuildedComponents/Forms/SignUpForm";
import BackgroundImageStyled from "../../components/StyledComponents/BackgroundImageStyled";
import FlexStyled from "../../components/StyledComponents/FlexStyled";

const SignUpPage: React.FC = () => {
    return(
        <FlexStyled>
            <BackgroundImageStyled />
            <SignUpForm />
        </FlexStyled>
    );
};

export default SignUpPage;
    