import { Link } from "react-router-dom";
import style from './ErrorPage.module.css';

const ErrorPage = () => {

    return(
        <div className={style.wrapper}>
            <div className={style.info}>
                <h2 className={style.title}>Something went wrong...</h2>
                <h2><Link className={style.error__link} to='/'>Home page</Link></h2>
            </div>
        </div> 
    )
}

export default ErrorPage;