import FlexStyled from '../../components/StyledComponents/FlexStyled';
import BackgroundImageStyled from '../../components/StyledComponents/BackgroundImageStyled';
import LoginForm from '../../components/BuildedComponents/Forms/LoginForm';

const LoginPage: React.FC = () => {
    return(
        <FlexStyled>
            <BackgroundImageStyled />
            <LoginForm />
        </FlexStyled>
    );
};

export default LoginPage;