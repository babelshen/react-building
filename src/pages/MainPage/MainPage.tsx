import React, { Suspense } from 'react';
import HeroSection from '../../components/BuildedComponents/HeroSection/HeroSection';
import AppWrapperStyled from '../../components/StyledComponents/AppWrapperStyled';
import Loader from '../../components/BuildedComponents/Loader';

const OpenDealSection = React.lazy(() => import('../../components/BuildedComponents/OpenDealSection'))

const MainPage: React.FC = () => {
    return(
        <main>
            <HeroSection />
            <Suspense fallback={<Loader />}>
                <AppWrapperStyled display='flex' direction='column' align='flex-start' margin='50px 80px 12px'>
                        <OpenDealSection />
                </AppWrapperStyled>
            </Suspense>
        </main>
    )
}

export default MainPage;