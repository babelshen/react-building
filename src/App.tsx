import { RouterProvider } from 'react-router'
import AppRoutes from './config/routeConfig';

function App() {
  return (     
    <RouterProvider router={AppRoutes} />
  )
}

export default App;